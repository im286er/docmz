<?php
if (!defined('THINK_PATH')) {
    exit();
}

// local variables has defined
// $db

// 保存文件
//tpx_rm_dir('./app/Common/Custom/data');
//tpx_copy_dir('./data', './app/Common/Custom/data');

// 需要备份的数据表
$tables_to_backup = array(
    'config',
);

tpx_rm_dir('./app/Common/Custom/dbinit/');
mkdir('./app/Common/Custom/dbinit/');
foreach ($tables_to_backup as $table) {
    file_put_contents('./app/Common/Custom/dbinit/' . $table . '.sql', db_get_insert_sqls($table));
}

echo 'ok';