<?php
if (!defined('THINK_PATH')) {
    exit();
}
define('ADMIN_PERMISSION_CHECK_IGNORE', true);

$err = error_reporting(E_ALL & (~E_NOTICE));
set_error_handler(function () {

});

$db = \Think\Db::getInstance();
$db_prefix = C('DB_PREFIX');
$db_type = C('DB_TYPE');
$tables = $db->getTables();

$msgs = array();


/////////////////////////////// app custom upgrade ///////////////////////////////

if (file_exists($file = './app/Common/Upgrade/custom.php')) {
    include $file;
}

/////////////////////////////// db repair or upgrade ///////////////////////////////

$table_name = $db_prefix . "cms_tag_pool";
if (!in_array($table_name, $tables)) {
    $db->execute("
						CREATE TABLE ${table_name} (
						  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
						  addtime INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
						  updatetime INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
						  cat VARCHAR(50) NOT NULL DEFAULT '' COMMENT '分类',
						  name VARCHAR(100) DEFAULT NULL DEFAULT '' COMMENT '名称',
						  PRIMARY KEY (id),
						  KEY addtime (addtime),
						  KEY updatetime (updatetime),
						  KEY cat (cat),
						  KEY name (name)
						) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT '标签池';");
    $msgs [] = '创建cms_tag_pool数据表';
}

$table_name = $db_prefix . "data_files";
if (in_array($table_name, $tables)) {
    $result = $db->query("DESC $table_name");
    if (is_array($result)) {
        $fields = array();
        foreach ($result as &$r) {
            $fields[] = $r['field'];
        }
        if (!in_array('filesize', $fields)) {
            $db->execute("ALTER TABLE $table_name ADD filesize INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小' AFTER uptime;");
            $msgs[] = 'data_files添加filesize字段';
        }
        if (!in_array('filename', $fields)) {
            $db->execute("ALTER TABLE $table_name ADD filename VARCHAR(100) NOT NULL DEFAULT '' COMMENT '文件名称' AFTER path;");
            $msgs[] = 'data_files添加filename字段';
        }
    }
}

/////////////////////////////// save old nodes & acl info ///////////////////////////////
$accessOld = D('AdminAccess')->field('role_id,node_id')->select();
$oldRoleNodes = array();
if (!empty($accessOld)) {
    foreach ($accessOld as $map) {
        $oldRoleNodes[] = array('role_id' => $map['role_id'], 'node_id' => $map['node_id']);
    }
}
$oldNodesIdPathMap = array();
$oldNodesPathIdMap = array();
$nodesOld = D('AdminNode')->select();
if (!empty($nodesOld)) {
    foreach ($nodesOld as &$node) {
        if ($node['level'] == 1) {
            $path = $node['name'];
            $oldNodesPathIdMap[$path] = $node['id'];
            $oldNodesIdPathMap[$node['id']] = $path;
        }
    }
    foreach ($nodesOld as &$node) {
        if ($node['level'] == 2) {
            if (!isset($oldNodesIdPathMap[$node['pid']])) {
                continue;
            }
            $path = $oldNodesIdPathMap[$node['pid']] . '/' . $node['name'];
            $oldNodesPathIdMap[$path] = $node['id'];
            $oldNodesIdPathMap[$node['id']] = $path;
        }
    }
    foreach ($nodesOld as &$node) {
        if ($node['level'] == 3) {
            if (!isset($oldNodesIdPathMap[$node['pid']])) {
                continue;
            }
            $path = $oldNodesIdPathMap[$node['pid']] . '/' . $node['name'];
            $oldNodesPathIdMap[$path] = $node['id'];
            $oldNodesIdPathMap[$node['id']] = $path;
        }
    }
}


// save the info into file in case of upgrade fail
//TODO

$db->execute("TRUNCATE TABLE ${db_prefix}admin_node");
$db->execute("TRUNCATE TABLE ${db_prefix}admin_access");

$modules = array(
    'Admin' => '技术后台',
    'Manage' => '运营后台'
);

$nodes = array();
foreach ($modules as $module => $moduleName) {
    $nodes[$module] = array();
    foreach (get_controllers($module, 'exportable') as $controller) {
        $controller_name = $controller ['name'];
        $class = "\\Admin\\Controller\\${controller_name}Controller";
        if (property_exists($class, 'export_menu')) {
            foreach ($class::$export_menu as $menus_global) {
                foreach ($menus_global as $controller_title => $menus) {
                    if (!isset ($nodes[$module] [$controller_name])) {
                        $nodes[$module] [$controller_name] = array(
                            'title' => $controller_title,
                            'sub' => array()
                        );
                    }
                    foreach ($menus as $method => $method_detial) {
                        $nodes[$module] [$controller_name] ['sub'] [$method] = $method_detial ['title'];
                        foreach ($method_detial ['hiddens'] as $method_hidden => $method_hidden_title) {
                            $nodes[$module] [$controller_name] ['sub'] [$method_hidden] = $method_hidden_title;
                        }
                    }
                }
            }
        }
    }
    if (empty($nodes[$module])) {
        unset($nodes[$module]);
    }
}

$nodePathIdMap = array();

/////////////////////////////// insert all the scanned nodes ///////////////////////////////
foreach ($nodes as $module => $moduleNodes) {
    $nodePathIdMap[$module] = M('AdminNode')->add(array(
        'name' => $module,
        'title' => empty($modules[$module]) ? $module : $modules[$module],
        'status' => 1,
        'sort' => 0,
        'pid' => 0,
        'level' => 1
    ));
    foreach ($moduleNodes as $controller => $controllerInfo) {
        $nodePathIdMap[$module . '/' . $controller] = M('AdminNode')->add(array(
            'name' => $controller,
            'title' => $controllerInfo ['title'],
            'status' => 1,
            'sort' => 1000,
            'pid' => $nodePathIdMap[$module],
            'level' => 2
        ));
        foreach ($controllerInfo['sub'] as $action => $actionName) {
            $nodePathIdMap[$module . '/' . $controller . '/' . $action] = M('AdminNode')->add(array(
                'name' => $action,
                'title' => $actionName,
                'status' => 1,
                'sort' => 1000,
                'pid' => $nodePathIdMap[$module . '/' . $controller],
                'level' => 3
            ));
        }

    }
}


/////////////////////////////// repair new & old node mapping ///////////////////////////////
foreach ($oldRoleNodes as &$r) {
    $roleId = $r['role_id'];
    $oldNodeId = $r['node_id'];
    $nodePath = $oldNodesIdPathMap[$oldNodeId];
    if (empty($nodePath)) {
        continue;
    }
    $nodeId = $nodePathIdMap[$nodePath];
    if (empty($nodeId)) {
        continue;
    }
    D('AdminAccess')->add(array(
        'role_id' => $roleId,
        'node_id' => $nodeId,
        'level' => count(explode('/', $nodePath)),
        'module' => ''
    ));
}

$msgs[] = '重新计算后台管理权限节点';

/////////////////////////////// init mod & cms ///////////////////////////////
foreach (get_controllers('Admin', 'mod') as $controller) {
    A('Admin/Mod' . $controller ['name'])->build($yummy = true);
}
foreach (get_controllers('Admin', 'cms') as $controller) {
    A('Admin/Cms' . $controller ['name'])->build($yummy = true);
}

/////////////////////////////// clear cache ///////////////////////////////
tpx_rm_dir('./_RUN/', false);
if (!file_exists('./_RUN/')) {
    @mkdir('./_RUN/');
}
@file_put_contents('./_RUN/index.html', 'Access Forbidden');

echo '<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>升级提示</title>
</head>
<body>
<div style="margin:0 auto;width:600px;background:#EEE;line-height:2em;font-size:12px;padding:1em;">
<div style="border-bottom:1px solid #CCC;font-size:14px;text-align: center;color:red;">升级提示</div>
<ul>
';
if (!empty($msgs)) {
    foreach ($msgs as $msg) {
        echo "<li>" . $msg . "</li>";
    }
    \Think\Log::write("Upgrade to V" . APP_VERSION . ', ' . join(",", $msgs) . "\r\n", \Think\Log::INFO);
}
echo '
<li>升级版本到 V' . APP_VERSION . '</li>
</ul>
</div>
</body>
</html>';
exit();



