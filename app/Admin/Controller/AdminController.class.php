<?php

namespace Admin\Controller;

use Think\Controller;

class AdminController extends Controller
{
    protected function _initialize()
    {

        if (file_exists($file = './app/Common/Custom/app_env_config.php')) {
            include $file;
        }

        tpx_upgrade_check();

        if (!defined('ADMIN_PERMISSION_CHECK_IGNORE')) {
            if (defined('ADMIN_UID')) {
                return;
            }
            define ('ADMIN_UID', I('session.' . C('USER_AUTH_KEY'), 0));


            // check installition
            if (CONTROLLER_NAME != 'Install' && (!file_exists('./_CFG/install.lock'))) {
                header('location: ' . __ROOT__ . '/' . C('ADMIN_SCRIPT') . '?s=/install/index');
                exit();
            }
            // check auth
            if (!in_array(CONTROLLER_NAME, array(
                    'Install',
                    'Login'
                ))
                && MODULE_NAME == 'Admin'
            ) {

                if (!ADMIN_UID) {
                    // for C('ADMIN_SCRIPT') upgrading
                    $currentScript = basename(I('server.SCRIPT_NAME'));
                    if (!empty($currentScript) && $currentScript != C('ADMIN_SCRIPT')) {
                        tpx_sys_config_set('ADMIN_SCRIPT', $currentScript);
                    }
                    header('location: ' . __ROOT__ . C('ADMIN_SCRIPT') . '?s=/login/index');
                    exit();
                }
                if (!access_permit()) {
                    if (MODULE_NAME == 'Admin' && CONTROLLER_NAME == 'System' && ACTION_NAME == 'info') {
                        // permit for basic info
                    } else {
                        $this->error(L('access_permissions'), __ROOT__ . C('ADMIN_SCRIPT') . '?s=/login/out');
                    }
                }
            }

        }
    }

    public function _empty($name)
    {
        $this->error(L('error_request'));
    }
}